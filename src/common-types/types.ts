export type UserType = {
  _id: string;
  fullName: string;
  isOnline: boolean;
  avatar: null | string;
};

export type MessageType = {
  _id: string;
  avatar: string;
  text: string;
  date: Date;
  user?: any;
  attachments?: Array<any>;
  audio?: string;
  isIncome: boolean;
  isReaded: boolean;
  isTyping: boolean;
};

export type DialogItemType = {
  _id: string;
  lastMessage: {
    text: string;
    isReaded: boolean;
    created_at: string;
    unreaded: number;
  };
  user: UserType;
};
