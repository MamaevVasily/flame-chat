import tinycolor from "tinycolor2";

const getCorrectIndex = (index: number) =>
  index > 255 ? 255 : index < 0 ? 0 : index;

export default (hash: string) => {
  const [r, g, b] = hash
    .substr(0, 3)
    .split("")
    .map((char) => getCorrectIndex(char.charCodeAt(0)));
  const color = tinycolor({ r, g, b }).lighten(20).saturate(30).toHexString();
  const colorLighten = tinycolor({ r, g, b })
    .lighten(50)
    .saturate()
    .toHexString();
  return { color, colorLighten };
};
