import React from "react";
import {
  TeamOutlined,
  FormOutlined,
  EllipsisOutlined,
} from "@ant-design/icons";
import Scrollbars from "react-custom-scrollbars";

import "./home.scss";
import DialogItems from "../../components/dialog-items/dialog-items";
import { dialogItemsMock, messagesMock } from "../../mock";
import Messages from "../../components/messages/messages";
import DialogInput from "../../components/dialog-input/dialog-input";
import DialogsSearch from "../../components/dialogs-search/dialogs-search";

const Home: React.FC = () => {
  return (
    <div className="chat">
      <div className="chat__sidebar">
        <div className="sidebar__header">
          <TeamOutlined className="header__icon --team" />
          <span>Список диалогов</span>
          <FormOutlined className="header__icon --form" />
        </div>
        <DialogsSearch className="sidebar__search" />
        <div className="sidebar__dialogs">
          <Scrollbars>
            <DialogItems
              items={dialogItemsMock}
              ownerId={"c4ca4238a0b923820dcc509a6f75849b"}
            />
          </Scrollbars>
        </div>
      </div>
      <div className="chat__dialog">
        <div className="dialog__header">
          <div className="header__status">
            <b className="status__name">Имя пользователя</b>
            {/* TODO: online/offline indicator */}
            <span className="status__indicator ">онлайн</span>
          </div>
          <EllipsisOutlined className="header__elipsis" />
        </div>
        <div className="dialog__messages">
          <Scrollbars>
            <Messages className="messages" messages={messagesMock} />
          </Scrollbars>
        </div>
        <DialogInput className="dialog__input" />
      </div>
    </div>
  );
};

export default Home;
