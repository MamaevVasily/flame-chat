import React from "react";
import formatDistanceToNow from "date-fns/formatDistanceToNow";
import ruLocale from "date-fns/locale/ru";
import classNames from "classnames";

import "./message.scss";
import ReadedIcon from "../../ui/readed-icon/readed-icon";
import { MessageType } from "../../../common-types/types";
import AudioMessage from "./audio-message";

type PropsType = {
  message: MessageType;
};

const Message: React.FC<{ message: MessageType }> = ({ message }) => {
  return (
    <div
      className={classNames("message", {
        "message--outcome": !message.isIncome,
        "message--is-typing": message.isTyping,
        "message--image":
          message.attachments && message.attachments.length === 1,
        "message--audio": message.audio,
      })}
    >
      {!message.isIncome && <ReadedIcon isReaded={message.isReaded} />}
      <div className="message__avatar">
        {/* FIXME: прокинуть пропс в alt */}
        <img src={message.avatar} alt="{`${props.user.fullName} avatar`}" />
      </div>
      <div className="message__content">
        {message.text && (
          <p className="content__text">{!message.isTyping && message.text}</p>
        )}
        {message.attachments && (
          <div className="content__attachments">
            {message.attachments.map((item) => (
              <div className="attachment" key={item.fileName}>
                <img src={item.url} alt={item.fileName} />
              </div>
            ))}
          </div>
        )}
        {message.isTyping && (
          <div className="content__typing-indicator">
            <span></span>
            <span></span>
            <span></span>
          </div>
        )}
        {message.audio && <AudioMessage message={message} />}
      </div>
      <time className="message__date">
        {message.isTyping
          ? "печатает..."
          : formatDistanceToNow(message.date, {
              addSuffix: true,
              locale: ruLocale,
            })}
      </time>
    </div>
  );
};

export default Message;
