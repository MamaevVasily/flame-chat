import React, { useRef, useState, useEffect } from "react";

import { MessageType } from "../../../common-types/types";
import formatTiming from "../../../utils/audio-timing-formater";

import WaveImg from "../../../assets/img/wave";
import playImg from "../../../assets/img/play-icon.svg";
import pauseImg from "../../../assets/img/pause-icon.svg";

const AudioMessage: React.FC<{ message: MessageType }> = ({ message }) => {
  const audioElem = useRef<HTMLAudioElement | null>(null);
  const [isPlaying, setIsPlaying] = useState(false);
  const [playingProgress, setPlayingProgress] = useState(0);
  const [playingTiming, setPlayingTiming] = useState("00:00");

  useEffect(() => {
    if (audioElem.current) {
      audioElem.current.addEventListener("playing", () => {
        setIsPlaying(true);
      });
      audioElem.current.addEventListener("ended", () => {
        setIsPlaying(false);
        setPlayingProgress(0);
        setPlayingTiming("00:00");
      });
      audioElem.current.addEventListener("pause", () => {
        setIsPlaying(false);
      });
      audioElem.current.addEventListener("timeupdate", () => {
        if (audioElem.current) {
          setPlayingTiming(formatTiming(audioElem.current.currentTime));
          setPlayingProgress(
            (audioElem.current.currentTime / audioElem.current.duration) * 100
          );
        }
      });
    }
    return () => {};
  }, []);

  const playHandler = () => {
    if (audioElem.current !== null) {
      if (isPlaying) {
        audioElem.current.pause();
        setIsPlaying(false);
      } else {
        audioElem.current.play();
        setIsPlaying(true);
      }
    }
  };

  return (
    <>
      <audio src={message.audio} ref={audioElem} preload="auto" />
      <div
        className="audio__progress"
        style={{ width: `${playingProgress}%` }}
      ></div>
      <div className="audio__play-btn">
        <button onClick={playHandler}>
          {isPlaying ? (
            <img src={pauseImg} alt="pause" />
          ) : (
            <img src={playImg} alt="play" />
          )}
        </button>
      </div>
      <WaveImg
        className="audio__wave-img"
        color={message.isIncome ? "white" : "grey"}
      />
      <span className="audio__duration">{playingTiming}</span>
    </>
  );
};

export default AudioMessage;
