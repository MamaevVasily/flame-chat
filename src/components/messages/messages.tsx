import React from "react";

import { MessageType } from "../../common-types/types";
import Message from "./message/message";

type PropTypes = {
  messages: Array<MessageType>;
  className: string;
};

const Messages: React.FC<PropTypes> = (props) => {
  return (
    <div className={props.className}>
      {props.messages.map((message: any) => (
        <Message message={message} key={message._id} />
      ))}
    </div>
  );
};

export default Messages;
