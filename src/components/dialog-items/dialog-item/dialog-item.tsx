import React from "react";
import classNames from "classnames";
import { format, isToday } from "date-fns";

import "./dialog-item.scss";
import ReadedIcon from "../../ui/readed-icon/readed-icon";
import { DialogItemType } from "../../../common-types/types";
import Avatar from "./avatar/avatar";

type PropsType = {
  item: DialogItemType;
  isOwner: boolean;
};

const getDate = (created_at: Date): string => {
  if (isToday(created_at)) {
    return format(created_at, "HH:mm");
  } else {
    return format(created_at, "d.MM.yyyy");
  }
};

const DialogItem = ({ item, isOwner }: PropsType) => {
  return (
    <div className="dialogs__item">
      <div
        className={classNames("item__avatar", {
          "--online": item.user.isOnline,
        })}
      >
        <Avatar
          avatar={item.user.avatar}
          id={item._id}
          userName={item.user.fullName}
        />
      </div>
      <div className="item__info">
        <div className="info__top">
          <b>{item.user.fullName}</b>
          {item.lastMessage.created_at && (
            <time>{getDate(new Date(item.lastMessage.created_at))}</time>
          )}
        </div>
        <div className="info__botttom">
          {item.lastMessage.text ? (
            <>
              <p>{item.lastMessage.text}</p>
              {isOwner ? (
                <ReadedIcon
                  className="botttom__thick-icon"
                  isReaded={item.lastMessage.isReaded}
                />
              ) : item.lastMessage.unreaded ? (
                <span className="botttom__unreaded-buble">
                  {item.lastMessage.unreaded > 99
                    ? "+99"
                    : item.lastMessage.unreaded}
                </span>
              ) : null}
            </>
          ) : (
            <p>нет сообщений</p>
          )}
        </div>
      </div>
    </div>
  );
};

export default DialogItem;
