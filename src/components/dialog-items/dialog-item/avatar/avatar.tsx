import React from "react";
import avatarColorGenerator from "../../../../utils/avatar-color-generator";
import "./avatar.scss";

type PropType = {
  avatar: string | null;
  id: string; // id пользователя в формате MD5 Hash
  userName: string;
};

const Avatar: React.FC<PropType> = ({ avatar, id, userName }) => {
  if (avatar) {
    return <img src={avatar} alt={userName} className="avatar" />;
  } else {
    const colors = avatarColorGenerator(id);
    const userInitails = () => {
      const fullNameArr = userName.toUpperCase().split(" ");
      return fullNameArr.length > 1
        ? fullNameArr[0][0] + fullNameArr[1][0]
        : fullNameArr[0][0];
    };
    return (
      <div
        className="avatar--empty"
        style={{
          background: `linear-gradient(300deg, ${colors.color} 0%, ${colors.colorLighten} 80%)`,
        }}
      >
        <span>{userInitails()}</span>
      </div>
    );
  }
};

export default Avatar;
