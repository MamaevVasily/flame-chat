import React from "react";
import DialogItem from "./dialog-item/dialog-item";
import { DialogItemType } from "../../common-types/types";
import orderBy from "lodash-es/orderBy";

type PropTypes = {
  items: Array<DialogItemType>;
  ownerId: string;
};

const DialogItems = (props: PropTypes) => {
  return (
    <div>
      {orderBy(props.items, ["lastMessage.created_at"], ["desc"]).map(
        (item) => (
          <DialogItem
            item={item}
            isOwner={item.user._id === props.ownerId}
            key={item._id}
          />
        )
      )}
    </div>
  );
};

export default DialogItems;
