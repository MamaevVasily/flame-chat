import React from "react";
import { Input, Button } from "antd";
import { CloseOutlined } from "@ant-design/icons";

import "./dialogs-search.scss";

type PropsType = {
  className: string;
};

const DialogsSearch: React.FC<PropsType> = (props) => {
  return (
    <div className={props.className}>
      <Input
        className="search-input"
        placeholder="Поиск среди контактов"
        suffix={
          <Button type="link" className="clear-button">
            <CloseOutlined className="clear-button__icon" />
          </Button>
        }
      />
    </div>
  );
};

export default DialogsSearch;
