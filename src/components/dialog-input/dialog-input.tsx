import React, { useState } from "react";
import { Input, Button } from "antd";
import {
  SmileOutlined,
  CameraOutlined,
  AudioOutlined,
  FileOutlined,
  SendOutlined,
} from "@ant-design/icons";

import "./dialog-input.scss";

type PropsType = {
  className: string;
};

const { TextArea } = Input;

const DialogInput: React.FC<PropsType> = (props) => {
  const [isDirty, setDirty] = useState(false);

  return (
    <div className={`input ${props.className}`}>
      <Button className="input__smile-btn" type="link">
        <SmileOutlined />
      </Button>
      <div className="input__textarea">
        <TextArea
          className="textarea__field"
          placeholder="Введите текст сообщения..."
          autoSize={{ minRows: 1, maxRows: 3 }}
        />
        <Button className="textarea__button" type="link">
          {isDirty ? <SendOutlined /> : <AudioOutlined />}
        </Button>
      </div>
      <div className="input__media-btn">
        <Button className="" type="link">
          <CameraOutlined />
        </Button>
        <Button className="" type="link">
          <FileOutlined />
        </Button>
      </div>
    </div>
  );
};

export default DialogInput;
