export const dialogItemsMock = [
  {
    _id: "5ee0aa20517cd2a07e1fb551",
    lastMessage: {
      text:
        "Ullamco consectetur magna amet laborum anim officia proident irure velit pariatur anim adipisicing reprehenderit dolor. Cupidatat esse consequat incididunt commodo esse tempor incididunt nisi ipsum esse laborum. Exercitation aliqua esse esse et tempor ut est qui deserunt id nulla quis aute enim.",
      isReaded: false,
      created_at:
        "Tue Oct 07 1997 17:30:55 GMT+0600 (Екатеринбург, летнее время)",
      unreaded: 4,
    },
    user: {
      _id: "5ee0aa20036533078cf3563a",
      fullName: "Haney Haynes",
      isOnline: true,
      avatar: null,
    },
  },
  {
    _id: "5ee0aa2013f046841e651dd0",
    lastMessage: {
      text:
        "Dolor culpa voluptate laboris ullamco sit qui cillum enim pariatur aliquip. Qui fugiat nostrud occaecat cillum culpa. Reprehenderit non nisi amet sint occaecat elit fugiat tempor quis do sint occaecat duis occaecat.",
      isReaded: false,
      created_at:
        "Tue Dec 11 1990 08:20:59 GMT+0500 (Екатеринбург, стандартное время)",
      unreaded: 4,
    },
    user: {
      _id: "5ee0aa2063678ad5fa0298bd",
      fullName: "Lindsey Patterson",
      isOnline: true,
      avatar: null,
    },
  },
  {
    _id: "5ee0aa2054b7707e01aba8e6",
    lastMessage: {
      text:
        "Officia cillum esse officia duis tempor consequat Lorem consectetur amet eiusmod et aliqua ad id. Laboris et sint laboris cupidatat occaecat occaecat deserunt qui ipsum aliqua ut. Labore adipisicing deserunt in excepteur commodo anim occaecat pariatur enim proident in.",
      isReaded: false,
      created_at:
        "Sun Jul 29 1990 09:06:10 GMT+0600 (Екатеринбург, летнее время)",
      unreaded: 1,
    },
    user: {
      _id: "5ee0aa201104705454526eb1",
      fullName: "Calhoun Stout",
      isOnline: true,
      avatar: null,
    },
  },
  {
    _id: "5ee0aa20e9dbb3bfb782365e",
    lastMessage: {
      text:
        "Dolore ut eiusmod officia excepteur esse consectetur laborum adipisicing eu voluptate. Ullamco sint nisi anim Lorem consequat deserunt veniam aliquip amet eiusmod irure quis. Reprehenderit sit do qui labore ipsum.",
      isReaded: false,
      created_at:
        "Sun May 29 2005 01:44:24 GMT+0600 (Екатеринбург, летнее время)",
      unreaded: 1,
    },
    user: {
      _id: "5ee0aa204abef7567546a48d",
      fullName: "Casey Hoover",
      isOnline: false,
      avatar: null,
    },
  },
  {
    _id: "5ee0aa2024455c3340aa3ad7",
    lastMessage: {
      text:
        "Eiusmod ex nulla qui fugiat sit id aliqua cupidatat commodo reprehenderit laborum. Deserunt exercitation dolor tempor ea incididunt excepteur proident sit. Aliquip velit laborum officia exercitation Lorem do tempor sit fugiat amet dolor sunt est amet.",
      isReaded: false,
      created_at:
        "Fri Oct 18 1991 09:13:08 GMT+0400 (Екатеринбург, стандартное время)",
      unreaded: 6,
    },
    user: {
      _id: "5ee0aa20d278fd5cd056ef23",
      fullName: "Hicks Jackson",
      isOnline: true,
      avatar: null,
    },
  },
  {
    _id: "5ee0aa20c131c839bb5eb2c3",
    lastMessage: {
      text:
        "Sit cillum dolor aliquip enim. Culpa mollit voluptate adipisicing adipisicing. Nostrud veniam ex minim nostrud magna officia deserunt commodo sunt elit adipisicing.",
      isReaded: false,
      created_at:
        "Thu Mar 06 2014 04:34:53 GMT+0600 (Екатеринбург, стандартное время)",
      unreaded: 0,
    },
    user: {
      _id: "5ee0aa206bf287924c4fc0e1",
      fullName: "Noel Noble",
      isOnline: true,
      avatar: null,
    },
  },
  {
    _id: "5ee0aa207b56c81629e0aef1",
    lastMessage: {
      text:
        "Elit elit ex eiusmod pariatur laboris velit exercitation consectetur commodo ullamco deserunt mollit anim. Excepteur sint culpa minim culpa nisi cillum ullamco ex eiusmod ad est nostrud incididunt magna. Anim commodo ipsum labore veniam esse enim reprehenderit quis exercitation.",
      isReaded: true,
      created_at:
        "Thu Feb 08 2001 10:46:46 GMT+0500 (Екатеринбург, стандартное время)",
      unreaded: 4,
    },
    user: {
      _id: "5ee0aa2006041ff9b1d65214",
      fullName: "Marlene Flowers",
      isOnline: true,
      avatar: null,
    },
  },
  {
    _id: "5ee0aa20a9cc40e4bd3c7490",
    lastMessage: {
      text:
        "Laboris ad occaecat dolore exercitation non sint aliqua. Occaecat sit fugiat est sit culpa amet labore do enim. Incididunt aute enim sint anim.",
      isReaded: false,
      created_at:
        "Sun Oct 28 1979 12:40:07 GMT+0500 (Екатеринбург, стандартное время)",
      unreaded: 7,
    },
    user: {
      _id: "5ee0aa2073266a05284f8191",
      fullName: "Joseph Perez",
      isOnline: true,
      avatar: null,
    },
  },
  {
    _id: "5ee0aa20dfdee0b5d5c55e78",
    lastMessage: {
      text:
        "Veniam mollit veniam enim nisi proident proident tempor sint velit fugiat. Duis et veniam proident sunt ex fugiat mollit ex reprehenderit duis. Do aliqua eu minim nisi.",
      isReaded: true,
      created_at:
        "Wed Dec 02 1998 21:27:16 GMT+0500 (Екатеринбург, стандартное время)",
      unreaded: 6,
    },
    user: {
      _id: "5ee0aa2062c852404c14a2d1",
      fullName: "Whitehead Bond",
      isOnline: true,
      avatar: null,
    },
  },
  {
    _id: "5ee0aa201908a06048ca8353",
    lastMessage: {
      text:
        "Irure nulla est consequat ut aute. In magna ea consequat dolore adipisicing aliquip mollit. Occaecat fugiat cillum dolor dolore id minim mollit ullamco excepteur laboris adipisicing aliqua.",
      isReaded: true,
      created_at:
        "Sun Apr 08 2007 00:28:00 GMT+0600 (Екатеринбург, летнее время)",
      unreaded: 2,
    },
    user: {
      _id: "5ee0aa20e292b647fd0a3299",
      fullName: "Marci Shaffer",
      isOnline: true,
      avatar: null,
    },
  },
  {
    _id: "5ee0aa2066d8903bd7c47905",
    lastMessage: {
      text:
        "Eu ut cillum eu reprehenderit veniam velit aliquip veniam ad aliquip officia voluptate culpa. Dolore velit officia aliquip fugiat eiusmod ut ipsum tempor. Voluptate consequat ex eu elit dolor anim consequat amet aliquip excepteur excepteur.",
      isReaded: false,
      created_at:
        "Tue Feb 11 2014 01:42:39 GMT+0600 (Екатеринбург, стандартное время)",
      unreaded: 7,
    },
    user: {
      _id: "5ee0aa204d5f4fdb1f5ebd08",
      fullName: "Maribel Burch",
      isOnline: true,
      avatar: null,
    },
  },
  {
    _id: "5ee0aa209a4cc554654e416a",
    lastMessage: {
      text:
        "Ut ipsum aliquip exercitation non labore ullamco esse quis consequat officia aliqua. Officia anim culpa quis adipisicing excepteur sit sint. Ullamco velit reprehenderit enim sit et eiusmod commodo ullamco consequat excepteur.",
      isReaded: true,
      created_at:
        "Sat Oct 11 2008 08:51:59 GMT+0600 (Екатеринбург, летнее время)",
      unreaded: 4,
    },
    user: {
      _id: "5ee0aa2033e6c19d4c1045fc",
      fullName: "Emilia Lott",
      isOnline: false,
      avatar: null,
    },
  },
  {
    _id: "5ee0aa203840ed272b438b2a",
    lastMessage: {
      text:
        "Ex labore dolor tempor anim officia id adipisicing velit veniam consectetur fugiat nulla ea. Eiusmod deserunt labore officia sunt minim est. Deserunt commodo anim sunt excepteur sint qui quis exercitation mollit voluptate adipisicing non mollit ut.",
      isReaded: true,
      created_at:
        "Wed Feb 13 2002 12:46:03 GMT+0500 (Екатеринбург, стандартное время)",
      unreaded: 1,
    },
    user: {
      _id: "5ee0aa200e222dafcd9617d5",
      fullName: "Herminia Gonzalez",
      isOnline: true,
      avatar: null,
    },
  },
  {
    _id: "5ee0aa20e2aca56effcc4cd6",
    lastMessage: {
      text:
        "Ut cupidatat duis magna nisi exercitation fugiat culpa. Adipisicing in eu ipsum nulla aute dolor esse culpa laborum esse incididunt ut ea consequat. Velit excepteur sit consequat ex deserunt sint ad Lorem minim velit quis esse.",
      isReaded: false,
      created_at:
        "Sun Jan 11 2004 08:29:29 GMT+0500 (Екатеринбург, стандартное время)",
      unreaded: 10,
    },
    user: {
      _id: "5ee0aa2084ecc50178b0e70a",
      fullName: "Diane Patel",
      isOnline: false,
      avatar: null,
    },
  },
  {
    _id: "5ee0aa20370d7878f5a48c85",
    lastMessage: {
      text:
        "Occaecat incididunt magna est ex pariatur incididunt duis. Excepteur ad incididunt cillum qui minim consequat aute ea deserunt. Eiusmod do deserunt dolor fugiat eiusmod velit.",
      isReaded: true,
      created_at:
        "Sun Nov 28 2004 06:01:55 GMT+0500 (Екатеринбург, стандартное время)",
      unreaded: 8,
    },
    user: {
      _id: "5ee0aa20fdd50b860e6f2daf",
      fullName: "Twila Ashley",
      isOnline: false,
      avatar: null,
    },
  },
  {
    _id: "5ee0aa208c2c0ba55575022e",
    lastMessage: {
      text:
        "Qui excepteur deserunt enim anim minim ad proident voluptate. Deserunt occaecat quis tempor consequat officia ad magna sunt. Magna officia veniam fugiat esse laboris enim.",
      isReaded: true,
      created_at:
        "Tue Oct 04 1988 19:26:20 GMT+0500 (Екатеринбург, стандартное время)",
      unreaded: 0,
    },
    user: {
      _id: "5ee0aa200d5ac6ca1bfe6166",
      fullName: "Robertson Wood",
      isOnline: false,
      avatar: null,
    },
  },
  {
    _id: "5ee0aa2017d01e407c96a750",
    lastMessage: {
      text:
        "Occaecat velit adipisicing nisi enim mollit laborum Lorem irure quis qui non. Magna dolor adipisicing nisi voluptate eiusmod sint nostrud id. Ullamco non culpa do aliquip non sint anim sit irure eu est cillum.",
      isReaded: false,
      created_at:
        "Thu May 12 2016 08:28:30 GMT+0500 (Екатеринбург, стандартное время)",
      unreaded: 5,
    },
    user: {
      _id: "5ee0aa2012b95009a85eba2a",
      fullName: "Cornelia Wilson",
      isOnline: true,
      avatar: null,
    },
  },
  {
    _id: "5ee0aa20644592b370497153",
    lastMessage: {
      text:
        "Et ullamco minim eu quis ullamco. Ex occaecat tempor velit quis labore laborum officia eiusmod. Velit incididunt labore irure magna ea cillum anim.",
      isReaded: false,
      created_at:
        "Sat Dec 07 1974 21:36:24 GMT+0500 (Екатеринбург, стандартное время)",
      unreaded: 6,
    },
    user: {
      _id: "5ee0aa20f2b0e4545e3316d6",
      fullName: "Walter Parrish",
      isOnline: true,
      avatar: null,
    },
  },
  {
    _id: "5ee0aa206111f4fcf82dbb03",
    lastMessage: {
      text:
        "Sint anim laboris duis consectetur est ipsum incididunt excepteur. Tempor irure esse nostrud ipsum magna amet id. Cupidatat ut Lorem commodo in deserunt enim exercitation proident esse aliquip irure.",
      isReaded: false,
      created_at:
        "Fri Jun 20 1980 06:18:06 GMT+0500 (Екатеринбург, стандартное время)",
      unreaded: 0,
    },
    user: {
      _id: "5ee0aa20747dac470dc8a0a9",
      fullName: "Evans Bright",
      isOnline: false,
      avatar: null,
    },
  },
  {
    _id: "5ee0aa20603cc3d499071f3a",
    lastMessage: {
      text:
        "Occaecat et tempor irure veniam reprehenderit. Est quis ullamco et exercitation ex tempor esse labore. Fugiat fugiat exercitation qui pariatur.",
      isReaded: false,
      created_at:
        "Fri May 18 2001 04:03:38 GMT+0600 (Екатеринбург, летнее время)",
      unreaded: 2,
    },
    user: {
      _id: "5ee0aa20163f155566ae5ddf",
      fullName: "Wong Campos",
      isOnline: false,
      avatar: null,
    },
  },
];

export const messagesMock = [
  {
    _id: "68890174e885ca2fc4da2502ed75c8ac",
    avatar:
      "https://avatars.mds.yandex.net/get-zen_doc/1900274/pub_5d6d517678125e00ad650e3a_5d6d51cd4735a600adbdd9c2/scale_1200",
    text: "test text",
    date: new Date("may 31, 2020 13:24:00"),
    isIncome: false,
    isReaded: true,
    isTyping: false,
    attachments: [
      {
        fileName: "",
        url: "https://source.unsplash.com/100x100/?random=1",
      },
      {
        fileName: "",
        url: "https://source.unsplash.com/100x100/?random=1",
      },
      {
        fileName: "",
        url: "https://source.unsplash.com/100x100/?random=1",
      },
      {
        fileName: "",
        url: "https://source.unsplash.com/100x100/?random=1",
      },
    ],
  },
  {
    _id: "647bcc5e578799f6708fe34ffa2ed8e8",
    avatar:
      "https://avatars.mds.yandex.net/get-zen_doc/1900274/pub_5d6d517678125e00ad650e3a_5d6d51cd4735a600adbdd9c2/scale_1200",
    text: "",
    date: new Date("may 31, 2020 13:25:00"),
    audio:
      "https://notificationsounds.com/soundfiles/941e1aaaba585b952b62c14a3a175a61/file-62_frog.mp3",
    isIncome: true,
    isReaded: true,
    isTyping: false,
  },
  {
    _id: "cb44b9c45e14bda39d2dd3f991174c6c",
    avatar:
      "https://avatars.mds.yandex.net/get-zen_doc/1900274/pub_5d6d517678125e00ad650e3a_5d6d51cd4735a600adbdd9c2/scale_1200",
    text: "",
    date: new Date("may 31, 2020 13:25:00"),
    audio:
      "https://notificationsounds.com/soundfiles/941e1aaaba585b952b62c14a3a175a61/file-62_frog.mp3",
    isIncome: false,
    isReaded: true,
    isTyping: false,
  },
  {
    _id: "cb44b9c45e14bda39d2dd3f991174c6c",
    avatar:
      "https://avatars.mds.yandex.net/get-zen_doc/1900274/pub_5d6d517678125e00ad650e3a_5d6d51cd4735a600adbdd9c2/scale_1200",
    text:
      "alskflkflkas;flkasjfkljklsa;sssssssskljd;aklsjdkl;asdkjasd;kasjd;klajsd;lajsdkl;ajsdslkasj;ljdslkajdas;ldja;sld",
    date: new Date("may 31, 2020 13:25:00"),
    isIncome: false,
    isReaded: true,
    isTyping: false,
  },
  {
    _id: "cb44b9c45e14bda39d2dd3f991174c6c",
    avatar:
      "https://avatars.mds.yandex.net/get-zen_doc/1900274/pub_5d6d517678125e00ad650e3a_5d6d51cd4735a600adbdd9c2/scale_1200",
    text:
      "alskflkflkas;flkasjfkljklsa;sssssssskljd;aklsjdkl;asdkjasd;kasjd;klajsd;lajsdkl;ajsdslkasj;ljdslkajdas;ldja;sld",
    date: new Date("may 31, 2020 13:25:00"),
    isIncome: false,
    isReaded: true,
    isTyping: false,
  },
  {
    _id: "cb44b9c45e14bda39d2dd3f991174c6c",
    avatar:
      "https://avatars.mds.yandex.net/get-zen_doc/1900274/pub_5d6d517678125e00ad650e3a_5d6d51cd4735a600adbdd9c2/scale_1200",
    text:
      "alskflkflkas;flkasjfkljklsa;sssssssskljd;aklsjdkl;asdkjasd;kasjd;klajsd;lajsdkl;ajsdslkasj;ljdslkajdas;ldja;sld",
    date: new Date("may 31, 2020 13:25:00"),
    isIncome: false,
    isReaded: true,
    isTyping: false,
  },
  {
    _id: "cb44b9c45e14bda39d2dd3f991174c6c",
    avatar:
      "https://avatars.mds.yandex.net/get-zen_doc/1900274/pub_5d6d517678125e00ad650e3a_5d6d51cd4735a600adbdd9c2/scale_1200",
    text:
      "alskflkflkas;flkasjfkljklsa;sssssssskljd;aklsjdkl;asdkjasd;kasjd;klajsd;lajsdkl;ajsdslkasj;ljdslkajdas;ldja;sld",
    date: new Date("may 31, 2020 13:25:00"),
    isIncome: false,
    isReaded: true,
    isTyping: false,
  },
  {
    _id: "cb44b9c45e14bda39d2dd3f991174c6c",
    avatar:
      "https://avatars.mds.yandex.net/get-zen_doc/1900274/pub_5d6d517678125e00ad650e3a_5d6d51cd4735a600adbdd9c2/scale_1200",
    text:
      "alskflkflkas;flkasjfkljklsa;sssssssskljd;aklsjdkl;asdkjasd;kasjd;klajsd;lajsdkl;ajsdslkasj;ljdslkajdas;ldja;sld",
    date: new Date("may 31, 2020 13:25:00"),
    isIncome: false,
    isReaded: true,
    isTyping: false,
  },
  {
    _id: "cb44b9c45e14bda39d2dd3f991174c6c",
    avatar:
      "https://avatars.mds.yandex.net/get-zen_doc/1900274/pub_5d6d517678125e00ad650e3a_5d6d51cd4735a600adbdd9c2/scale_1200",
    text:
      "alskflkflkas;flkasjfkljklsa;sssssssskljd;aklsjdkl;asdkjasd;kasjd;klajsd;lajsdkl;ajsdslkasj;ljdslkajdas;ldja;sld",
    date: new Date("may 31, 2020 13:25:00"),
    isIncome: false,
    isReaded: true,
    isTyping: false,
  },
  {
    _id: "cb44b9c45e14bda39d2dd3f991174c6c",
    avatar:
      "https://avatars.mds.yandex.net/get-zen_doc/1900274/pub_5d6d517678125e00ad650e3a_5d6d51cd4735a600adbdd9c2/scale_1200",
    text:
      "alskflkflkas;flkasjfkljklsa;sssssssskljd;aklsjdkl;asdkjasd;kasjd;klajsd;lajsdkl;ajsdslkasj;ljdslkajdas;ldja;sld",
    date: new Date("may 31, 2020 13:25:00"),
    isIncome: false,
    isReaded: true,
    isTyping: false,
  },
  {
    _id: "cb44b9c45e14bda39d2dd3f991174c6c",
    avatar:
      "https://avatars.mds.yandex.net/get-zen_doc/1900274/pub_5d6d517678125e00ad650e3a_5d6d51cd4735a600adbdd9c2/scale_1200",
    text:
      "alskflkflkas;flkasjfkljklsa;sssssssskljd;aklsjdkl;asdkjasd;kasjd;klajsd;lajsdkl;ajsdslkasj;ljdslkajdas;ldja;sld",
    date: new Date("may 31, 2020 13:25:00"),
    isIncome: false,
    isReaded: true,
    isTyping: false,
  },
  {
    _id: "cb44b9c45e14bda39d2dd3f991174c6c",
    avatar:
      "https://avatars.mds.yandex.net/get-zen_doc/1900274/pub_5d6d517678125e00ad650e3a_5d6d51cd4735a600adbdd9c2/scale_1200",
    text:
      "alskflkflkas;flkasjfkljklsa;sssssssskljd;aklsjdkl;asdkjasd;kasjd;klajsd;lajsdkl;ajsdslkasj;ljdslkajdas;ldja;sld",
    date: new Date("may 31, 2020 13:25:00"),
    isIncome: false,
    isReaded: true,
    isTyping: false,
  },
  {
    _id: "cb44b9c45e14bda39d2dd3f991174c6c",
    avatar:
      "https://avatars.mds.yandex.net/get-zen_doc/1900274/pub_5d6d517678125e00ad650e3a_5d6d51cd4735a600adbdd9c2/scale_1200",
    text:
      "alskflkflkas;flkasjfkljklsa;sssssssskljd;aklsjdkl;asdkjasd;kasjd;klajsd;lajsdkl;ajsdslkasj;ljdslkajdas;ldja;sld",
    date: new Date("may 31, 2020 13:25:00"),
    isIncome: false,
    isReaded: true,
    isTyping: false,
  },
];
//<Message
//   avatar="https://avatars.mds.yandex.net/get-zen_doc/1900274/pub_5d6d517678125e00ad650e3a_5d6d51cd4735a600adbdd9c2/scale_1200"
//   text="test texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest text"
//   date={Date.now()}
//   isIncome={true}
//   isReaded={false}
//   attachments={[
//     {
//       fileName: "",
//       url: "https://source.unsplash.com/100x100/?random=1",
//     },
//     {
//       fileName: "",
//       url: "https://source.unsplash.com/100x100/?random=1",
//     },
//     {
//       fileName: "",
//       url: "https://source.unsplash.com/100x100/?random=1",
//     },
//     {
//       fileName: "",
//       url: "https://source.unsplash.com/100x100/?random=1",
//     },
//   ]}
//   isTyping={false}
// />
// <Message
//   avatar="https://avatars.mds.yandex.net/get-zen_doc/1900274/pub_5d6d517678125e00ad650e3a_5d6d51cd4735a600adbdd9c2/scale_1200"
//   text="test texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest texttest text
//   "
//   date={Date.now()}
//   isIncome={false}
//   isReaded={false}
//   isTyping={false}
// />
// <Message
//   avatar="https://avatars.mds.yandex.net/get-zen_doc/1900274/pub_5d6d517678125e00ad650e3a_5d6d51cd4735a600adbdd9c2/scale_1200"
//   text={null}
//   date={Date.now()}
//   isIncome={true}
//   isTyping={true}
//   isReaded={false}
// />
// <Message
//   avatar="https://avatars.mds.yandex.net/get-zen_doc/1900274/pub_5d6d517678125e00ad650e3a_5d6d51cd4735a600adbdd9c2/scale_1200"
//   text={null}
//   date={Date.now()}
//   isIncome={false}
//   isTyping={true}
//   isReaded={false}
// />
// <Message
//   avatar="https://avatars.mds.yandex.net/get-zen_doc/1900274/pub_5d6d517678125e00ad650e3a_5d6d51cd4735a600adbdd9c2/scale_1200"
//   text={null}
//   date={Date.now()}
//   attachments={[
//     {
//       fileName: "",
//       url: "https://source.unsplash.com/200x200/?random=1",
//     },
//   ]}
//   isIncome={true}
//   isTyping={false}
//   isReaded={false}
// />
// <Message
//   avatar="https://avatars.mds.yandex.net/get-zen_doc/1900274/pub_5d6d517678125e00ad650e3a_5d6d51cd4735a600adbdd9c2/scale_1200"
//   text={null}
//   date={Date.now()}
//   attachments={[
//     {
//       fileName: "",
//       url: "https://source.unsplash.com/200x200/?random=1",
//     },
//   ]}
//   isIncome={false}
//   isTyping={false}
//   isReaded={false}
